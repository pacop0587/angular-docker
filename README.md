# AngularApp

Este proyecto es un ejemplo de un entorno de desarrollo para Angular dentro de un contenedor Docker.

# Requisitos

- Tener instalado Docker
- Tener instalado Docker-Compose

# Instrucciones

1. git clone https://pacop0587@bitbucket.org/pacop0587/angular-docker.git

2. cd angular-docker/

3. docker-compose up -d --build

4. En el navegador escribir "localhost:4200"
